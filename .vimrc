execute pathogen#infect()
"runtime! ftplugin/man.vim   " man pages in vim
" opens man pages inside vim for word under cursor
"nnoremap K :Man <cword><CR>

filetype plugin indent on

" highlight 81th column
set colorcolumn=81

set listchars=tab:>.,trail:.
set list

"autocmd ColorScheme *
"colorscheme torte "murphy, industry
highlight ColorColumn ctermbg=grey guibg=grey
highlight Search cterm=bold ctermbg=grey guibg=LightBlue
"highlight Normal ctermbg=black ctermfg=white
highlight SpecialKey ctermbg=lightblue ctermfg=red

syntax on

" open new vertical split to the left from current one
set splitright

set noshowmode  "put jedi helpers into cmd line
set nowrap
set mouse=a
set tabstop=2
set nosmartindent
set shiftwidth=2
set expandtab
set softtabstop=2

set foldmethod=indent   "fold lines with equal indent
set foldlevel=99        "dont fold lines automaticaly on start

set bg=light

set hlsearch

set tags=./tags,tags;./jtags,jtags;


"syntax checking
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_python_python_exec = '/usr/bin/python3'
"let g:syntastic_python_checkers = ['flake8']

"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0

"exclude c, c++
let g:syntastic_mode_map = { 'passive_filetypes': ['c', 'cpp'] }


"toggle line folding
nnoremap <space> za
"toggle tagbar
nmap <F9> :TagbarToggle<CR>

nnoremap <F5> :!screen /dev/ttyUSB0 115200<CR>
nnoremap <F6> :make test -j4<CR>
nnoremap <F7> :set paste!<CR>
nnoremap <F8> :w<CR> :!clear<CR> :make -j4<CR>
nnoremap <F9> :!make flash -j4 && screen /dev/ttyUSB0 115200<CR>

let g:jedi#show_call_signatures = 2
